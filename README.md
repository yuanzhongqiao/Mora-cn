<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mora：更像 Sora，用于通用视频生成</font></font></h1><a id="user-content-mora-more-like-sora-for-generalist-video-generation" class="anchor" aria-label="永久链接：Mora：更像 Sora，用于通用视频生成" href="#mora-more-like-sora-for-generalist-video-generation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔍 请参阅我们最新的视频生成论文：</font></font><a href="http://arxiv.org/abs/2403.13248" rel="nofollow"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">“Mora：通过多代理框架实现通才视频生成”</font></font></strong></a> <font style="vertical-align: inherit;"><a href="https://github.com/lichao-sun/Mora"><font style="vertical-align: inherit;">）</font></a></font><a href="http://arxiv.org/abs/2403.13248" rel="nofollow"><img src="https://camo.githubusercontent.com/9a81bdf1776b4ef8fa45a092fb6d2134ed5f497e331c0d8460ee29c61bfc452f/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f50617065722d2546302539462538452539332d6c69676874626c75653f7374796c653d666c61742d737175617265" alt="纸" data-canonical-src="https://img.shields.io/badge/Paper-%F0%9F%8E%93-lightblue?style=flat-square" style="max-width: 100%;"></a> <a href="https://github.com/lichao-sun/Mora"><img src="https://camo.githubusercontent.com/051cd72007f1e848772fa3825eed0b4763e27e04169022ea4055a95fc030d20a/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4774696875622d2546302539462538452539332d6c69676874626c75653f7374796c653d666c61742d737175617265" alt="GitHub" data-canonical-src="https://img.shields.io/badge/Gtihub-%F0%9F%8E%93-lightblue?style=flat-square" style="max-width: 100%;"><font style="vertical-align: inherit;"></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📧 如果您发现错误或有任何建议，请通过电子邮件告知我们：</font></font><a href="mailto:lis221@lehigh.edu"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">lis221@lehigh.edu</font></font></a></p>
</blockquote>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📰新闻</font></font></h2><a id="user-content-news" class="anchor" aria-label="永久链接：📰新闻" href="#news"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🚀️ 3 月 20 日：我们的论文“ </font></font><a href="https://arxiv.org/abs/2403.13248" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mora: Enabling Generalist Video Generation via A Multi-Agent Framework</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ”发布！</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是莫拉</font></font></h2><a id="user-content-what-is-mora" class="anchor" aria-label="永久链接：什么是莫拉" href="#what-is-mora"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mora 是一个多代理框架，旨在利用多个视觉代理的协作方法来促进通用视频生成任务。</font><font style="vertical-align: inherit;">它旨在复制和扩展 OpenAI Sora 的功能。
</font></font><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task.jpg"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task.jpg" alt="任务" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🎥演示（1024×576分辨率，12秒以上！）</font></font></h2><a id="user-content-demo-1024576-resolution-12-seconds-and-more" class="anchor" aria-label="永久链接：🎥演示（1024×576 分辨率，12 秒甚至更多！）" href="#demo-1024576-resolution-12-seconds-and-more"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="left" dir="auto">
  <animated-image data-catalyst="" style="width: 49%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/demo1.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/demo1.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
     
  <animated-image data-catalyst="" style="width: 49%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/demo2.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/demo2.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      
  <animated-image data-catalyst="" style="width: 49%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/demo3.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/demo3.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      
  <animated-image data-catalyst="" style="width: 49%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/demo4.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/demo4.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
     
</p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mora：视频生成的多代理框架</font></font></h2><a id="user-content-mora-a-multi-agent-framework-for-video-generation" class="anchor" aria-label="永久链接：Mora：视频生成的多代理框架" href="#mora-a-multi-agent-framework-for-video-generation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/method.jpg"><img src="https://github.com/lichao-sun/Mora/raw/main/image/method.jpg" alt="测试图像" style="max-width: 100%;"></a></p>
<ul dir="auto">
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多代理协作</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：利用多个先进的视觉人工智能代理，每个代理专门负责视频生成过程的不同方面，以在各种任务中实现高质量的结果。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">广泛的任务</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：能够执行文本到视频生成、文本条件图像到视频生成、扩展生成的视频、视频到视频编辑、连接视频以及模拟数字世界，从而涵盖广泛的范围视频生成应用程序。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开源和可扩展</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Mora 的开源性质促进了社区内的创新和协作，从而实现持续改进和定制。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">经验证的性能</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：实验结果表明，Mora 在各种任务中能够实现接近 Sora 的性能，使其成为视频生成领域引人注目的开源替代方案。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">结果</font></font></h2><a id="user-content-results" class="anchor" aria-label="永久链接：结果" href="#results"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本到视频生成</font></font></h3><a id="user-content-text-to-video-generation" class="anchor" aria-label="永久链接：文本到视频生成" href="#text-to-video-generation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<tbody><tr>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">输入提示</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">输出视频</font></font></b></th>
</tr>
<tr>
  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">水晶般清澈的蓝色海洋下，充满生机的珊瑚礁充满生机，色彩缤纷的鱼儿在珊瑚间游动，阳光透过水面，轻柔的水流移动着海洋植物。</font></font></td>
  <td><animated-image data-catalyst="" style="width: 480px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task_1_demo_1.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task_1_demo_1.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      
       
</tr>
<tr>
  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">雄伟的山脉被白雪覆盖，山峰触云，山脚清澈见底的湖泊，倒映着山峦和天空，形成了一幅令人惊叹的天然镜子。</font></font></td>
  <td><animated-image data-catalyst="" style="width: 480px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task_1_demo_2.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task_1_demo_2.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
     
      </span></animated-image></td>
</tr>
  <tr>
  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">浩瀚的沙漠之中，一座金色的沙漠之城出现在地平线上，它的建筑融合了古埃及和未来元素。这座城市被辐射能量屏障包围，而在空中，有七道光柱环绕。</font></font></td>
  <td><animated-image data-catalyst="" style="width: 480px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task_1_demo_3.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task_1_demo_3.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
       </animated-image></td>
</tr>
</tbody></table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本条件图像到视频生成</font></font></h3><a id="user-content-text-conditional-image-to-video-generation" class="anchor" aria-label="永久链接：文本条件图像到视频生成" href="#text-conditional-image-to-video-generation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<tbody><tr>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">输入提示</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">输入图像</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">莫拉生成的视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">索拉生成的视频</font></font></b></th>
</tr>
<tr>
  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不同怪物家族的平面设计风格的怪物插图。</font><font style="vertical-align: inherit;">该群体包括一个毛茸茸的棕色怪物、一个带有天线的光滑黑色怪物、一个有斑点的绿色怪物和一个微小的圆点怪物，所有这些怪物都在一个有趣的环境中互动。</font></font></td>
  <td><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/input1.jpg"><img src="https://github.com/lichao-sun/Mora/raw/main/image/input1.jpg" width="600" height="90" style="max-width: 100%;"></a></td>
  <td><animated-image data-catalyst="" style="width: 160px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task2_demo1.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task2_demo1.gif" height="90" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 160px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/sora_demo1.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/sora_demo1.gif" height="90" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      </animated-image></td>
</tr>
<tr>
  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">写有“SORA”的现实云的图像。</font></font></td>
  <td><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/input2.jpg"><img src="https://github.com/lichao-sun/Mora/raw/main/image/input2.jpg" width="600" height="90" style="max-width: 100%;"></a></td>
  <td><animated-image data-catalyst="" style="width: 160px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task2_demo2.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task2_demo2.gif" height="90" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 160px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/sora_demo2.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/sora_demo2.gif" height="90" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
     </animated-image></td>
</tr>
</tbody></table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">扩展生成的视频</font></font></h3><a id="user-content-extend-generated-video" class="anchor" aria-label="永久链接：扩展生成的视频" href="#extend-generated-video"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<tbody><tr>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原创视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">莫拉加长视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">索拉延长视频</font></font></b></th>
</tr>
<tr>
  <td><animated-image data-catalyst="" style="width: 330px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/original video.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/original video.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 330px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/mora_task3.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/mora_task3.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
      </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 330px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task3_sora.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task3_sora.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      </animated-image></td>
</tr>
</tbody></table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频到视频编辑</font></font></h3><a id="user-content-video-to-video-editing" class="anchor" aria-label="永久链接：视频到视频编辑" href="#video-to-video-editing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<tbody><tr>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">操作说明</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原创视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">莫拉编辑视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">空编辑的视频</font></font></b></th>
</tr>
<tr>
  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将场景更改为 1920 年代的旧校车。</font><font style="vertical-align: inherit;">确保保持红色。</font></font></td>
  <td><animated-image data-catalyst="" style="width: 240px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task4_original.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task4_original.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
    </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 240px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task4_mora_1920.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task4_mora_1920.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 240px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task4_sora_1920.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task4_sora_1920.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
    </animated-image></td>
</tr>
<tr>
  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将视频放到有彩虹路的太空中</font></font></td>
  <td><animated-image data-catalyst="" style="width: 240px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task4_original.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task4_original.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 240px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task4_mora_rainbow.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task4_mora_rainbow.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
    </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 240px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task4_sora_rainbow.gif" data-target="animated-image.originalLink"><img src="https://github.com/lichao-sun/Mora/raw/main/image/task4_sora_rainbow.gif" height="auto" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      </animated-image></td>
</tr>
</tbody></table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连接视频</font></font></h3><a id="user-content-connect-videos" class="anchor" aria-label="永久链接：连接视频" href="#connect-videos"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<tbody><tr>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">输入上一个视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">输入下一个视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">输出连接视频</font></font></b></th>
</tr>
<tr>
  <td><animated-image data-catalyst="" style="width: 300px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task5_mora1.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task5_mora1.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
      </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 300px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task5_mora2.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task5_mora2.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 300px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task5_mora.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task5_mora.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
</tr>
<tr>
  <td><animated-image data-catalyst="" style="width: 300px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task5_sora1.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task5_sora1.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 300px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task5_sora2.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task5_sora2.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 300px;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task5_sora.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task5_sora.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
</tr>
</tbody></table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模拟数字世界</font></font></h3><a id="user-content-simulate-digital-worlds" class="anchor" aria-label="永久链接：模拟数字世界" href="#simulate-digital-worlds"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<tbody><tr>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">莫拉模拟视频</font></font></b></th>
  <th align="left"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">索拉模拟视频</font></font></b></th>
</tr>
<tr>
  <td><animated-image data-catalyst="" style="width: 100%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task6_mora1.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task6_mora1.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 100%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task6_sora1.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task6_sora1.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
</tr>
<tr>
  <td><animated-image data-catalyst="" style="width: 100%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task6_mora2.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task6_mora2.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
       </animated-image></td>
  <td><animated-image data-catalyst="" style="width: 100%;"><a target="_blank" rel="noopener noreferrer" href="https://github.com/lichao-sun/Mora/blob/main/image/task6_sora2.gif" data-target="animated-image.originalLink" hidden=""><img src="https://github.com/lichao-sun/Mora/raw/main/image/task6_sora2.gif" height="auto" style="max-width: 100%;" data-target="animated-image.originalImage" hidden=""></a>
     </animated-image></td>
</tr>
</tbody></table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></h2><a id="user-content-getting-started" class="anchor" aria-label="永久链接：开始使用" href="#getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码将尽快发布！</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引文</font></font></h2><a id="user-content-citation" class="anchor" aria-label="永久链接：引文" href="#citation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>@article{yuan2024mora,
  title={Mora: Enabling Generalist Video Generation via A Multi-Agent Framework},
  author={Yuan, Zhengqing and Chen, Ruoxi and Li, Zhaoxu and Jia, Haolong and He, Lifang and Wang, Chi and Sun, Lichao},
  journal={arXiv preprint arXiv:2403.13248},
  year={2024}
}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@article{yuan2024mora,
  title={Mora: Enabling Generalist Video Generation via A Multi-Agent Framework},
  author={Yuan, Zhengqing and Chen, Ruoxi and Li, Zhaoxu and Jia, Haolong and He, Lifang and Wang, Chi and Sun, Lichao},
  journal={arXiv preprint arXiv:2403.13248},
  year={2024}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>@article{liu2024sora,
  title={Sora: A Review on Background, Technology, Limitations, and Opportunities of Large Vision Models},
  author={Liu, Yixin and Zhang, Kai and Li, Yuan and Yan, Zhiling and Gao, Chujie and Chen, Ruoxi and Yuan, Zhengqing and Huang, Yue and Sun, Hanchi and Gao, Jianfeng and others},
  journal={arXiv preprint arXiv:2402.17177},
  year={2024}
}
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>@misc{openai2024sorareport,
  title={Video generation models as world simulators},
  author={OpenAI},
  year={2024},
  howpublished={https://openai.com/research/video-generation-models-as-world-simulators},
}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@misc{openai2024sorareport,
  title={Video generation models as world simulators},
  author={OpenAI},
  year={2024},
  howpublished={https://openai.com/research/video-generation-models-as-world-simulators},
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</article></div>
